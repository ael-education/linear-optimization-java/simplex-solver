/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package ru.ael.math.simplexsolverexample;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.math3.exception.TooManyIterationsException;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.LinearConstraint;
import org.apache.commons.math3.optim.linear.LinearConstraintSet;
import org.apache.commons.math3.optim.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optim.linear.NoFeasibleSolutionException;
import org.apache.commons.math3.optim.linear.NonNegativeConstraint;
import org.apache.commons.math3.optim.linear.Relationship;
import org.apache.commons.math3.optim.linear.SimplexSolver;
import org.apache.commons.math3.optim.linear.UnboundedSolutionException;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

/**
 *
 * @author vaganovdv
 */
public class SimplexSolverExample {

    public static void main(String[] args) {
        System.out.println("Пример решения задачи линейного программирования");

        /*
        
    Теоретический материал: стр. 37 (издание 7), стр. 30 (издание 6).
    Источник теста: Введение в исследование операций Хэмди А. Таха, Издательский дом Вильямс,Санкт-Петербург, 2001
    Задача стр. 316 из упражнения 7.5,с № 3a (раздел 7. Теория линейного программирования")
    
    Исходные данные теста:
    Минимизировать целевую функцию: 
    GoalTypeValue = GoalType.MINIMIZE;
    
    Формулировка задачи: решить задачу ЛП методом решения задач с огрниченными переменными.
    Минимизировать целевую функцию z (переменные x1, x2, x3)
    
        z = 6 * x1 - 2 * x2 - 3 * x3
    
    при ограничениях:
         x1       x2      x3    Lconst      Rconst  
    --------------------------------------------------------    
    |   2x1  +   4x2 +   2x3 + Lconst[0] <= 8       // 1
    |    x1  -   2x2 +   3x3 + Lconst[1] <= 7       // 2
    |    x1                  + Lconst[2] >= 0;      // 3  code =2
    |    x1                  + Lconst[2] <= 2;      // 4  code =3  
    |            x2          + Lconst[2] >= 0;      // 5
    |            x2          + Lconst[2] <= 2;      // 6
    |                    x3  + Lconst[2] >= 0;      // 7
    |                    x3  + Lconst[2] <= 1;      // 8  
    
    Результат оптимизации:
    x1 = 0.0 
    x2 = 3/2
    x3 = 1
    z = -6
    
         */
        
        // Вектор коэффциентов целевой функции
        //                                             x1    x2     x3
        double[] objectiveFunctionСoeff = new double[]{6.0, -2.0, -3.0};
        // Постоянный член линейного уравнения целевой функции
        double[] objectiveFunctionСonstantTermArray = {0.0};

        // Входной массив L - левая часть коэффциентов 
        double[][] L
                = new double[][]{
                    new double[]{2.0, 4.0, 2.0},    // 1
                    new double[]{1.0, -2.0, 3.0},   // 2
                    new double[]{1.0, 0.0, 0.0},    // 3
                    new double[]{1.0, 0.0, 0.0},    // 4
                    new double[]{0.0, 1.0, 0.0},    // 5
                    new double[]{0.0, 1.0, 0.0},    // 6
                    new double[]{0.0, 0.0, 1.0},    // 7
                    new double[]{0.0, 0.0, 1.0},    // 8
                };

        // Входной массив R (все коэффициенты равны нулю - коэффциенты правой части отсутвуют)
        double[][] R
                = new double[][]{
                    new double[]{0.0, 0.0, 0.0}, // 1
                    new double[]{0.0, 0.0, 0.0}, // 2
                    new double[]{0.0, 0.0, 0.0}, // 3
                    new double[]{0.0, 0.0, 0.0}, // 4
                    new double[]{0.0, 0.0, 0.0}, // 5
                    new double[]{0.0, 0.0, 0.0}, // 6
                    new double[]{0.0, 0.0, 0.0}, // 7
                    new double[]{0.0, 0.0, 0.0}, // 8
                };

        //  В левой части отсутсвуют коэффциенты Lconst заполнен нулями 
        //                               1    2    3    4    5    6    7    8
        double[] Lconst = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        // Правая часть неравенства Rconst
        //                             1    2    3    4    5    6    7    8
        double[] Rconst = new double[]{8.0, 7.0, 0.0, 2.0, 0.0, 2.0, 0.0, 1.0};

        // Вектор решения Z   ( для контроля корректности работы программы)
        // Из учебника на стр. 316 (6 издание)
        double[] Xsolution = new double[]{0.0, 3 / 2, 1.00};        
        double Zsolution = 6.0;

       
        
        NonNegativeConstraint negativeConstraint = new NonNegativeConstraint(true);
       
        
        GoalType goalType;                            // Вид оптимизации (минимизация, максимизация)
        NonNegativeConstraint negative;               // Переменная "Признак допустимости отрицательных переменных

        PointValuePair simplexSolution = null;
        SimplexSolver solver = new SimplexSolver();

        LinearObjectiveFunction linearObjectiveFunction = new LinearObjectiveFunction(objectiveFunctionСoeff, objectiveFunctionСonstantTermArray[0]);
        Collection<LinearConstraint> constraints = new ArrayList<LinearConstraint>();
        
        //  Знаки между левой и правой частями  в матрице ограничений 
        //                                   1    2    3    4    5    6    7    8
        //                                  <=   <=   >=   <=    >=  <=   >=   <=
        double[] relation   = new double[]{ 3.0, 3.0, 2.0, 3.0, 2.0, 3.0, 2.0, 3.0 };
        
                // Построение массива ограничений по матрице - размер матрицы берется по размеру матрицы L или матрицы R (размеры матриц одинаковые всегда)                    
        for (int i = 0; i < L.length; i++) { // Чтение строки матрицы ограничений 
            
            double[] leftCoeff = new double[L[0].length];   // Массив строки для формирования ограничения  (левая часть)
            double[] rightCoeff = new double[R[0].length];  // Массив строки для формирования ограничения (правая часть)
            double leftConst = Lconst[i];          // Константа в левой части для текущей строки   
            double rightConst = Rconst[i];         // Констрнта в правой части для текущей строки 

            for (int j = 0; j < L[0].length; j++) {
                // Формирование строки коэффциентов левой части неравенства
                leftCoeff[j] = L[i][j];
            }
            for (int k = 0; k < R[0].length; k++) {
                // Формирование строки коэффциентов правой части неравенства
                rightCoeff[k] = R[i][k];
            }

            // Знак =
            if (relation[i] == 1) {
                constraints.add(new LinearConstraint(leftCoeff, leftConst, Relationship.EQ, rightCoeff, rightConst));
            }

            // Знак >=
            if (relation[i] == 2) {
                constraints.add(new LinearConstraint(leftCoeff, leftConst, Relationship.GEQ, rightCoeff, rightConst));
            }

            // Знак <=
            if (relation[i] == 3) {
                constraints.add(new LinearConstraint(leftCoeff, leftConst, Relationship.LEQ, rightCoeff, rightConst));
            }

        }
        
        
        
        // Поиск оптимального решения
        try {
            
            PointValuePair solution = solver.optimize(new MaxIter(5000), linearObjectiveFunction, new LinearConstraintSet(constraints), GoalType.MINIMIZE, negativeConstraint);
            double[] point = solution.getPoint();
            System.out.println("Получен вектор решения размерностью ["+point.length+"] элементов");
            // Печать решения 
            for (int i = 0; i < point.length; i++) {
                System.out.println("x ["+i+"] = "+point[i]);                
            }
            
            
        } catch (TooManyIterationsException ex) {
            System.out.println("Решение не получено: превышено допустимое количество итераций");
        } catch (UnboundedSolutionException ex) {
            System.out.println("Решение не получено: расчетные значения уходят в бесконечность");
        } catch (NoFeasibleSolutionException ex) {
            System.out.println("Решение не получено: отсутвует решение оптимизациеонной задачи");
        }

    }
}
